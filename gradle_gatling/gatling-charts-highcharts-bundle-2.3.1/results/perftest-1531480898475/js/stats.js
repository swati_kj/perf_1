var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "393",
        "ok": "393",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2150",
        "ok": "2150",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1186",
        "ok": "1186",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "332",
        "ok": "332",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1164",
        "ok": "1165",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1405",
        "ok": "1405",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1763",
        "ok": "1763",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 134,
        "percentage": 13
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 403,
        "percentage": 40
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 463,
        "percentage": 46
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "250",
        "ok": "250",
        "ko": "-"
    }
},
contents: {
"req_request-0-684d2": {
        type: "REQUEST",
        name: "request_0",
path: "request_0",
pathFormatted: "req_request-0-684d2",
stats: {
    "name": "request_0",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "393",
        "ok": "393",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2150",
        "ok": "2150",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1186",
        "ok": "1186",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "332",
        "ok": "332",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1165",
        "ok": "1164",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1405",
        "ok": "1403",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1763",
        "ok": "1763",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 134,
        "percentage": 13
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 403,
        "percentage": 40
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 463,
        "percentage": 46
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "250",
        "ok": "250",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
