var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "13",
        "ok": "13",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "897",
        "ok": "897",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "323",
        "ok": "323",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "188",
        "ok": "188",
        "ko": "-"
    },
    "percentiles1": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "percentiles2": {
        "total": "311",
        "ok": "311",
        "ko": "-"
    },
    "percentiles3": {
        "total": "646",
        "ok": "646",
        "ko": "-"
    },
    "percentiles4": {
        "total": "847",
        "ok": "847",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 12,
        "percentage": 92
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 1,
        "percentage": 8
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.481",
        "ok": "0.481",
        "ko": "-"
    }
},
contents: {
"req_request-1-46da4": {
        type: "REQUEST",
        name: "request_1",
path: "request_1",
pathFormatted: "req_request-1-46da4",
stats: {
    "name": "request_1",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "897",
        "ok": "897",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "897",
        "ok": "897",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "897",
        "ok": "897",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "897",
        "ok": "897",
        "ko": "-"
    },
    "percentiles2": {
        "total": "897",
        "ok": "897",
        "ko": "-"
    },
    "percentiles3": {
        "total": "897",
        "ok": "897",
        "ko": "-"
    },
    "percentiles4": {
        "total": "897",
        "ok": "897",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 1,
        "percentage": 100
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.037",
        "ok": "0.037",
        "ko": "-"
    }
}
    },"req_request-1-redir-7e85b": {
        type: "REQUEST",
        name: "request_1 Redirect 1",
path: "request_1 Redirect 1",
pathFormatted: "req_request-1-redir-7e85b",
stats: {
    "name": "request_1 Redirect 1",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "311",
        "ok": "311",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "311",
        "ok": "311",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "311",
        "ok": "311",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "311",
        "ok": "311",
        "ko": "-"
    },
    "percentiles2": {
        "total": "311",
        "ok": "311",
        "ko": "-"
    },
    "percentiles3": {
        "total": "311",
        "ok": "311",
        "ko": "-"
    },
    "percentiles4": {
        "total": "311",
        "ok": "311",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.037",
        "ok": "0.037",
        "ko": "-"
    }
}
    },"req_request-2-93baf": {
        type: "REQUEST",
        name: "request_2",
path: "request_2",
pathFormatted: "req_request-2-93baf",
stats: {
    "name": "request_2",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "478",
        "ok": "478",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "478",
        "ok": "478",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "478",
        "ok": "478",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "478",
        "ok": "478",
        "ko": "-"
    },
    "percentiles2": {
        "total": "478",
        "ok": "478",
        "ko": "-"
    },
    "percentiles3": {
        "total": "478",
        "ok": "478",
        "ko": "-"
    },
    "percentiles4": {
        "total": "478",
        "ok": "478",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.037",
        "ok": "0.037",
        "ko": "-"
    }
}
    },"req_request-3-d0973": {
        type: "REQUEST",
        name: "request_3",
path: "request_3",
pathFormatted: "req_request-3-d0973",
stats: {
    "name": "request_3",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "percentiles2": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "percentiles3": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "percentiles4": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.037",
        "ok": "0.037",
        "ko": "-"
    }
}
    },"req_request-4-e7d1b": {
        type: "REQUEST",
        name: "request_4",
path: "request_4",
pathFormatted: "req_request-4-e7d1b",
stats: {
    "name": "request_4",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "percentiles2": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "percentiles3": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "percentiles4": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.037",
        "ok": "0.037",
        "ko": "-"
    }
}
    },"req_request-4-redir-036f2": {
        type: "REQUEST",
        name: "request_4 Redirect 1",
path: "request_4 Redirect 1",
pathFormatted: "req_request-4-redir-036f2",
stats: {
    "name": "request_4 Redirect 1",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "204",
        "ok": "204",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "204",
        "ok": "204",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "204",
        "ok": "204",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "204",
        "ok": "204",
        "ko": "-"
    },
    "percentiles2": {
        "total": "204",
        "ok": "204",
        "ko": "-"
    },
    "percentiles3": {
        "total": "204",
        "ok": "204",
        "ko": "-"
    },
    "percentiles4": {
        "total": "204",
        "ok": "204",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.037",
        "ok": "0.037",
        "ko": "-"
    }
}
    },"req_request-5-48829": {
        type: "REQUEST",
        name: "request_5",
path: "request_5",
pathFormatted: "req_request-5-48829",
stats: {
    "name": "request_5",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "292",
        "ok": "292",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "292",
        "ok": "292",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "292",
        "ok": "292",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "292",
        "ok": "292",
        "ko": "-"
    },
    "percentiles2": {
        "total": "292",
        "ok": "292",
        "ko": "-"
    },
    "percentiles3": {
        "total": "292",
        "ok": "292",
        "ko": "-"
    },
    "percentiles4": {
        "total": "292",
        "ok": "292",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.037",
        "ok": "0.037",
        "ko": "-"
    }
}
    },"req_request-6-027a9": {
        type: "REQUEST",
        name: "request_6",
path: "request_6",
pathFormatted: "req_request-6-027a9",
stats: {
    "name": "request_6",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "267",
        "ok": "267",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "267",
        "ok": "267",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "267",
        "ok": "267",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "267",
        "ok": "267",
        "ko": "-"
    },
    "percentiles2": {
        "total": "267",
        "ok": "267",
        "ko": "-"
    },
    "percentiles3": {
        "total": "267",
        "ok": "267",
        "ko": "-"
    },
    "percentiles4": {
        "total": "267",
        "ok": "267",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.037",
        "ok": "0.037",
        "ko": "-"
    }
}
    },"req_request-7-f222f": {
        type: "REQUEST",
        name: "request_7",
path: "request_7",
pathFormatted: "req_request-7-f222f",
stats: {
    "name": "request_7",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "percentiles2": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "percentiles3": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "percentiles4": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.037",
        "ok": "0.037",
        "ko": "-"
    }
}
    },"req_request-8-ef0c8": {
        type: "REQUEST",
        name: "request_8",
path: "request_8",
pathFormatted: "req_request-8-ef0c8",
stats: {
    "name": "request_8",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "percentiles2": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "percentiles3": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "percentiles4": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.037",
        "ok": "0.037",
        "ko": "-"
    }
}
    },"req_request-9-d127e": {
        type: "REQUEST",
        name: "request_9",
path: "request_9",
pathFormatted: "req_request-9-d127e",
stats: {
    "name": "request_9",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "453",
        "ok": "453",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "453",
        "ok": "453",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "453",
        "ok": "453",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "453",
        "ok": "453",
        "ko": "-"
    },
    "percentiles2": {
        "total": "453",
        "ok": "453",
        "ko": "-"
    },
    "percentiles3": {
        "total": "453",
        "ok": "453",
        "ko": "-"
    },
    "percentiles4": {
        "total": "453",
        "ok": "453",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.037",
        "ok": "0.037",
        "ko": "-"
    }
}
    },"req_request-10-1cfbe": {
        type: "REQUEST",
        name: "request_10",
path: "request_10",
pathFormatted: "req_request-10-1cfbe",
stats: {
    "name": "request_10",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles2": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles3": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles4": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.037",
        "ok": "0.037",
        "ko": "-"
    }
}
    },"req_request-10-redi-69a19": {
        type: "REQUEST",
        name: "request_10 Redirect 1",
path: "request_10 Redirect 1",
pathFormatted: "req_request-10-redi-69a19",
stats: {
    "name": "request_10 Redirect 1",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "percentiles2": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "percentiles3": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "percentiles4": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.037",
        "ok": "0.037",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
