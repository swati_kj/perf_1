var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "1030",
        "ok": "40",
        "ko": "990"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "146",
        "ko": "0"
    },
    "maxResponseTime": {
        "total": "2512",
        "ok": "2512",
        "ko": "651"
    },
    "meanResponseTime": {
        "total": "74",
        "ok": "937",
        "ko": "39"
    },
    "standardDeviation": {
        "total": "267",
        "ok": "728",
        "ko": "147"
    },
    "percentiles1": {
        "total": "0",
        "ok": "793",
        "ko": "0"
    },
    "percentiles2": {
        "total": "0",
        "ok": "1167",
        "ko": "0"
    },
    "percentiles3": {
        "total": "590",
        "ok": "2353",
        "ko": "579"
    },
    "percentiles4": {
        "total": "972",
        "ok": "2464",
        "ko": "610"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 21,
        "percentage": 2
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 9,
        "percentage": 1
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 10,
        "percentage": 1
    },
    "group4": {
        "name": "failed",
        "count": 990,
        "percentage": 96
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "93.636",
        "ok": "3.636",
        "ko": "90"
    }
},
contents: {
"req_request-0-684d2": {
        type: "REQUEST",
        name: "request_0",
path: "request_0",
pathFormatted: "req_request-0-684d2",
stats: {
    "name": "request_0",
    "numberOfRequests": {
        "total": "1000",
        "ok": "10",
        "ko": "990"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "786",
        "ko": "0"
    },
    "maxResponseTime": {
        "total": "982",
        "ok": "982",
        "ko": "651"
    },
    "meanResponseTime": {
        "total": "47",
        "ok": "894",
        "ko": "39"
    },
    "standardDeviation": {
        "total": "169",
        "ok": "64",
        "ko": "147"
    },
    "percentiles1": {
        "total": "0",
        "ok": "915",
        "ko": "0"
    },
    "percentiles2": {
        "total": "0",
        "ok": "940",
        "ko": "0"
    },
    "percentiles3": {
        "total": "582",
        "ok": "966",
        "ko": "579"
    },
    "percentiles4": {
        "total": "652",
        "ok": "979",
        "ko": "610"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 2,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 8,
        "percentage": 1
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 990,
        "percentage": 99
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "90.909",
        "ok": "0.909",
        "ko": "90"
    }
}
    },"req_adsbygoogle-js-e15e4": {
        type: "REQUEST",
        name: "adsbygoogle.js",
path: "adsbygoogle.js",
pathFormatted: "req_adsbygoogle-js-e15e4",
stats: {
    "name": "adsbygoogle.js",
    "numberOfRequests": {
        "total": "10",
        "ok": "10",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "288",
        "ok": "288",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "200",
        "ok": "200",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "percentiles1": {
        "total": "193",
        "ok": "193",
        "ko": "-"
    },
    "percentiles2": {
        "total": "204",
        "ok": "204",
        "ko": "-"
    },
    "percentiles3": {
        "total": "263",
        "ok": "263",
        "ko": "-"
    },
    "percentiles4": {
        "total": "283",
        "ok": "283",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 10,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.909",
        "ok": "0.909",
        "ko": "-"
    }
}
    },"req_angular-js-682a0": {
        type: "REQUEST",
        name: "angular.js",
path: "angular.js",
pathFormatted: "req_angular-js-682a0",
stats: {
    "name": "angular.js",
    "numberOfRequests": {
        "total": "10",
        "ok": "10",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1721",
        "ok": "1721",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2512",
        "ok": "2512",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2094",
        "ok": "2094",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "277",
        "ok": "277",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2175",
        "ok": "2175",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2318",
        "ok": "2318",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2456",
        "ok": "2456",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2501",
        "ok": "2501",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 10,
        "percentage": 100
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.909",
        "ok": "0.909",
        "ko": "-"
    }
}
    },"req_bootstrap-min-c-5b8a7": {
        type: "REQUEST",
        name: "bootstrap.min.css",
path: "bootstrap.min.css",
pathFormatted: "req_bootstrap-min-c-5b8a7",
stats: {
    "name": "bootstrap.min.css",
    "numberOfRequests": {
        "total": "10",
        "ok": "10",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "855",
        "ok": "855",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "563",
        "ok": "563",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "126",
        "ok": "126",
        "ko": "-"
    },
    "percentiles1": {
        "total": "524",
        "ok": "524",
        "ko": "-"
    },
    "percentiles2": {
        "total": "639",
        "ok": "639",
        "ko": "-"
    },
    "percentiles3": {
        "total": "767",
        "ok": "767",
        "ko": "-"
    },
    "percentiles4": {
        "total": "837",
        "ok": "837",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 9,
        "percentage": 90
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 1,
        "percentage": 10
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.909",
        "ok": "0.909",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
