import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

class perftest extends Simulation {
   private val baseUrl = "https://jsonplaceholder.typicode.com"
   private val uri = "https://jsonplaceholder.typicode.com/posts/1"
   private val contentType = "application/json"
   private val endpoint = "/posts/1"
   private val requestCount = 1000


 val httpProtocol: HttpProtocolBuilder = http
   .baseURL(baseUrl)
   .contentTypeHeader(contentType)
    .acceptEncodingHeader("gzip") 
   val headers_0 = Map("Expect" -> "100-continue")

 val scn: ScenarioBuilder = scenario("RecordedSimulation")
   .exec(http("request_0")
     .get(endpoint)
     .headers(headers_0)
     .check(status.is(200)))

 setUp(scn.inject(atOnceUsers(requestCount))).protocols(httpProtocol)
}