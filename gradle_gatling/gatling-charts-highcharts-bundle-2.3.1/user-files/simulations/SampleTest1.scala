import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

class SampleTest1 extends Simulation {
   private val baseUrl = "http://dummy.restapiexample.com"
  // private val basicAuthHeader = "Basic YmxhemU6UTF3MmUzcjQ="
  // private val authPass = "Q1w2e3r4"
   private val uri = "https://jsonplaceholder.typicode.com/posts/1"
   private val contentType = "application/json"
   private val endpoint = "/posts/1"
   //private val authUser= "blaze"
   private val requestCount = 1000


 val httpProtocol: HttpProtocolBuilder = http
   .baseURL(baseUrl)
   .contentTypeHeader(contentType)
    .acceptEncodingHeader("gzip") 
   val headers_0 = Map("Expect" -> "100-continue")

 val scn: ScenarioBuilder = scenario("RecordedSimulation")
   .exec(http("request_0")
     .get(endpoint)
     .headers(headers_0)
     .check(status.is(200)))

 setUp(scn.inject(atOnceUsers(requestCount))).protocols(httpProtocol)
}